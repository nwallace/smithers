require_relative "spec_helper"

Smithers::Container.start :persistence

Dir[SPEC_ROOT.join("support/db/*.rb").to_s].each(&method(:require))
Dir[SPEC_ROOT.join("shared/db/*.rb").to_s].each(&method(:require))

require "database_cleaner"
static_tables = %w[services event_types reaction_types event_parameter_types reaction_parameter_types]
DatabaseCleaner[:sequel, connection: Test::DatabaseHelpers.db].strategy = :truncation, { except: static_tables }

RSpec.configure do |config|
  config.include Test::DatabaseHelpers

  config.before :suite do
    DatabaseCleaner.clean_with(:truncation, except: static_tables)
  end

  config.around :each do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end
end
