require "spec_helper"
require "smithers/models/integration"
require_file_under_test!

RSpec.describe Smithers::Main::Operations::Webhooks::Execute do
  describe "#call" do
    let(:integration) { instance_double(Smithers::Models::Integration) }
    let(:event) { double("The event") }

    it "continues successfully when the integration authorizes the event" do
      expect(integration).to receive(:execute!).with(event)
      r = subject.call(integration: integration, event: event)
      expect(r).to be_success
      expect(r).not_to be_failure
      expect(r.value).to eq(http_status_code: 200, message: "Complete")
    end

    it "aborts unsuccessfully when the integration barres the event" do
      expect(integration).to receive(:execute!).with(event).and_raise ":("
      r = subject.call(integration: integration, event: event)
      expect(r).not_to be_success
      expect(r).to be_failure
      expect(r.value).to eq(http_status_code: 500, message: "Unexpected error: RuntimeError - :(")
    end
  end
end
