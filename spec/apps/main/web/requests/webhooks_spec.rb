require "web_spec_helper"

RSpec.describe Smithers::Main::Web, type: :request do

  describe "POST /api/v1/webhooks/:id" do
    context "for a Gitlab-to-Matrix integration" do
      let(:matrix_server)   { env_var(:matrix_server) }
      let(:matrix_token)    { env_var(:matrix_access_token) }
      let(:matrix_room_id)  { env_var(:matrix_room_id) }
      let(:matrix_response) { instance_double(HTTParty::Response, success?: true) }
      let(:matrix_api) { instance_double(Smithers::Matrix::API, post_message: matrix_response) }
      let(:status) { "success" }
      let(:builds) { [] }
      let(:params) { {
        object_kind: "pipeline",
        object_attributes: {
          id: 25,
          status: status,
          ref: "master",
          sha: "6e28a2b480ff77ef3ce89cde7d86aedd16951fb9",
        },
        project: {
          name: "smithers",
          web_url: "https://git.nosuchthingastwo.com/nathan/smithers",
        },
        commit: {
          message: "add Matrix API for posting messages\n",
        },
        builds: builds,
      } }
      let(:gitlab_token) { env_var(:gitlab_secret_token) }
      let(:integration) { Factory[:integration, event_type: gitlab_pipeline_event_type,
                                  reaction_type: post_matrix_message_reaction_type] }

      before do
        header("X-Gitlab-Token", gitlab_token)
        allow(Smithers::Matrix::API).to receive(:new).and_return(matrix_api)
        Factory[:reaction_parameter, integration: integration,
                reaction_parameter_type: matrix_server_param_type, value: matrix_server]
        Factory[:reaction_parameter, integration: integration,
                reaction_parameter_type: matrix_room_id_param_type, value: matrix_room_id]
        Factory[:reaction_parameter, integration: integration,
                reaction_parameter_type: matrix_access_token_param_type, value: matrix_token]
        Factory[:event_parameter, integration: integration,
                event_parameter_type: gitlab_secret_token_param_type, value: gitlab_token]
      end

      it "requires a valid integration id" do
        post_json described_route(id: "0"), params
        expect(last_response.status).to eq 404
      end

      it "requires the correct authentication" do
        header("X-Gitlab-Token", "BOGUS")
        post_json described_route(id: integration.id), params
        expect(last_response.status).to eq 401
      end

      context "that succeeded" do
        it "posts a 'Build succeeded' message to Matrix" do
          expect(Smithers::Matrix::API).to receive(:new)
            .with(server: matrix_server, access_token: matrix_token)
            .and_return(matrix_api)
          expect(matrix_api).to receive(:post_message)
            .with(matrix_room_id, a_string_starting_with("*<font color='seagreen'>Build succeeded:</font>*"))
            .and_return(matrix_response)
          post_json described_route(id: integration.id), params
          expect(last_response).to be_ok
        end
      end

      context "that failed" do
        let(:status) { "failed" }
        let(:builds) { [ { id: 49, status: "failed"  } ] }

        it "posts a 'Build failed' message to Matrix" do
          expect(Smithers::Matrix::API).to receive(:new)
            .with(server: matrix_server, access_token: matrix_token)
            .and_return(matrix_api)
          expect(matrix_api).to receive(:post_message)
            .with(matrix_room_id, a_string_starting_with("**<font color='red'>Build failed:</font>**"))
            .and_return(matrix_response)
          post_json described_route(id: integration.id), params
          expect(last_response).to be_ok
        end
      end

      context "that just started" do
        let(:status) { "pending" }
        it "doesn't post anything to Matrix" do
          expect(Smithers::Matrix::API).not_to receive(:new)
          post_json described_route(id: integration.id), params
          expect(last_response).to be_ok
        end
      end

      context "when the message fails to post to Matrix" do
        let(:matrix_response) { instance_double(HTTParty::Response, success?: false, code: 400, body: ":(") }
        it "returns an error with details" do
          post_json described_route(id: integration.id), params
          expect(last_response).to be_server_error
          expect(last_response_json).to eq(status: "failed", message: "Unexpected error: RuntimeError - Unable to post message to Matrix: HTTP 400 - :(")
        end
      end
    end

    context "for a Mixer-to-Discord integration" do
      let(:discord_api) { instance_double(Smithers::Discord::API, post_message: discord_response) }
      let(:discord_response) { instance_double(HTTParty::Response, success?: true) }
      let(:channel_id) { "8389395" }
      let(:discord_channel_id) { "123456" }
      let(:poker_signature) { "sha384=810FBD8205A222FCF119F9DF3FE3883298E932B021B41D04A808D3CD080D6D232E233C5B6B1DEC269C309B94B0DC259A" }
      let(:request_body) { %Q|{"event":"channel:#{channel_id}:update","id":"5600313a-622b-4e2d-8775-66b5d40cc614","payload":{"online":true},"sentAt":"2018-03-14T11:27:12.6831014+00:00"}| }
      let(:integration) { Factory[:integration, event_type: mixer_channel_update_event_type,
                                  reaction_type: post_discord_message_reaction_type] }
      let(:params) { JSON.parse(request_body) }

      before do
        allow(Smithers::Discord::API).to receive(:new).and_return(discord_api)
        header("Poker-Signature", poker_signature)
        Factory[:event_parameter, integration: integration,
                event_parameter_type: mixer_events_list_param_type,
                value: %Q|["channel:#{channel_id}:update"]|]
        Factory[:reaction_parameter, integration: integration,
                reaction_parameter_type: discord_channel_id_param_type, value: discord_channel_id]
      end

      it "requires a valid integration id" do
        post_json described_route(id: "0"), params
        expect(last_response.status).to eq 404
      end

      it "requires the correct signature for authorization" do
        header("Poker-Signature", "BOGUS")
        post_json described_route(id: integration.id), params
        expect(last_response.status).to eq 401
      end

      it "posts a 'Now streaming' message to Discord when a channel comes online" do
        expect(Smithers::Discord::API).to receive(:new)
          .with(no_args)
          .and_return(discord_api)
        expect(discord_api).to receive(:post_message)
          .with(discord_channel_id, a_string_starting_with("**DoofyPoo** is streaming"))
          .and_return(discord_response)
        post_json described_route(id: integration.id), params
        expect(last_response).to be_ok
      end

      it "rejects events it's not listening for" do
        expect(Smithers::Discord::API).not_to receive(:new)
        allow_any_instance_of(Smithers::Mixer::API).to receive(:signature_valid?).and_return(true)
        post_json described_route(id: integration.id), params.merge("event" => "channel:#{channel_id}:broadcast")
        expect(last_response).to be_bad_request
      end

      it "ignores the event when the channel hasn't just come online" do
        expect(Smithers::Discord::API).not_to receive(:new)
        allow_any_instance_of(Smithers::Mixer::API).to receive(:signature_valid?).and_return(true)
        post_json described_route(id: integration.id), params.merge("payload" => {"online" => false})
        expect(last_response).to be_ok
      end

      context "when the message fails to post to Discord" do
        let(:discord_response) { instance_double(HTTParty::Response, success?: false, code: 400, body: ":(") }
        it "returns an error with details" do
          post_json described_route(id: integration.id), params
          expect(last_response).to be_server_error
          expect(last_response_json).to eq(status: "failed", message: "Unexpected error: RuntimeError - Unable to post message to Discord: HTTP 400 - :(")
        end
      end
    end
  end
end
