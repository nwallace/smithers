Factory.define :service do |f|
  f.name { fake(:app, :name) }
  f.slug { |name| name.downcase.gsub(/\W/, "") }
  f.timestamps
end
