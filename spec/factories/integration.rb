Factory.define :integration do |f|
  f.association :event_type
  f.association :reaction_type
  f.timestamps
end
