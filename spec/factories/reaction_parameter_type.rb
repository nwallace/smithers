Factory.define :reaction_parameter_type do |f|
  f.association :reaction_type
  f.type "MatrixAccessToken"
  f.timestamps
end
