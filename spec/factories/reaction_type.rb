Factory.define :reaction_type do |f|
  f.association :service
  f.type "PostMatrixMessage"
  f.timestamps
end
