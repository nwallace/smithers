Factory.define :event_parameter do |f|
  f.association :integration
  f.association :event_parameter_type
  f.value { fake(:internet, :password) }
  f.timestamps
end
