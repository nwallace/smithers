Factory.define :reaction_parameter do |f|
  f.association :integration
  f.association :reaction_parameter_type
  f.value { fake(:internet, :password) }
  f.timestamps
end
