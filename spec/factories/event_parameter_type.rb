Factory.define :event_parameter_type do |f|
  f.association :event_type
  f.type "GitlabSecretToken"
  f.timestamps
end
