Factory.define :event_type do |f|
  f.association :service
  f.type "GitlabPipelineEvent"
  f.inlet "webhook"
  f.timestamps
end
