require "db_spec_helper"
require_file_under_test!
require "smithers/models/event_types/gitlab_pipeline_event"

RSpec.describe Persistence::Relations::Integrations do
  let(:repo) { Smithers::Container["repositories.integrations"].integrations }

  describe ".by_id" do
    let(:integration) { Factory[:integration, event_type: gitlab_pipeline_event_type,
                                reaction_type: post_matrix_message_reaction_type] }

    it "returns the integration for the given id" do
      result = repo.by_id(integration.id)
      expect(result.id).to eq integration.id
      expect(result.event_type_id).to eq integration.event_type_id
      expect(result.reaction_type_id).to eq integration.reaction_type_id
    end

    it "brings along the integration's parameters and services" do
      skip "This fails when run together with other specs (but not in isolation). Something to do with TypedModel interacting with ROM::Struct#attributes"
      Factory[:event_parameter, integration: integration,
              event_parameter_type: gitlab_secret_token_param_type, value: "The value"]
      Factory[:reaction_parameter, integration: integration,
              reaction_parameter_type: matrix_server_param_type, value: "Other value"]
      result = repo.by_id(integration.id)
      expect(result.fetch_event_parameter(gitlab_secret_token_param_type)).to eq "The value"
      expect(result.fetch_reaction_parameter(matrix_server_param_type)).to eq "Other value"
      expect(result.event_service).to match_struct gitlab_service
      expect(result.reaction_service).to match_struct matrix_service
    end
  end
end
