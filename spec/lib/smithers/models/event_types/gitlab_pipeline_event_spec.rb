require "spec_helper"
require_file_under_test!

RSpec.describe Smithers::Models::EventTypes::GitlabPipelineEvent do

  describe "#new_webhook_event" do
    it "returns an event initialized with the given parameters and request" do
      event_params = double("Event params")
      request = double("The request")
      event = double("The event")
      expect(described_class::WebhookEvent).to receive(:new)
        .with(event_params: event_params, request: request)
        .and_return(event)
      expect(subject.new_webhook_event(event_params, request)).to eq event
    end
  end

  describe described_class::WebhookEvent do
    let(:secret_token) { "Shhh!" }
    let(:data) { double("The request data") }
    let(:request) { double("The request", get_header: secret_token, params: data) }
    let(:event_params) { [
      double(value: "Something else ;)", type: "GitlabUsername"),
      double(value: "Shhh!", type: "GitlabSecretToken"),
    ] }
    let(:presenter) { instance_double(Smithers::Gitlab::Presenters::PipelineEvent,
                                      status: "success", to_markdown: ":)") }

    subject { described_class.new(event_params: event_params, request: request) }

    before do
      allow(Smithers::Gitlab::Presenters).to receive(:present_event)
        .with(data)
        .and_return(presenter)
    end

    describe "initialization" do
      it "initializes :request_params to contain the secret token from the header and the request params" do
        expect(request).to receive(:get_header)
          .with("HTTP_X_GITLAB_TOKEN")
          .and_return "It's a secret!"
        subject = described_class.new(event_params: event_params, request: request)
        expect(subject.request_params).to eq(secret_token: "It's a secret!", event_data: data)
      end

      it "initializes :request_params secret token to nil when the secret token is missing" do
        expect(request).to receive(:get_header).with("HTTP_X_GITLAB_TOKEN").and_return nil
        subject = described_class.new(event_params: event_params, request: request)
        expect(subject.request_params).to eq(secret_token: nil, event_data: data)
      end
    end

    describe "#authorized?" do
      it "is true when the request's secret token matches the event's" do
        expect(subject).to be_authorized
      end

      it "is false when the request's secret token doesn't match the event's" do
        subject.request_params.merge!(secret_token: "Bogus")
        expect(subject).not_to be_authorized
      end

      it "is false when the request's secret token is missing" do
        subject.request_params.merge!(secret_token: nil)
        expect(subject).not_to be_authorized
      end
    end

    describe "#valid?" do
      let(:data) { {
        "object_kind" => "pipeline",
        "object_attributes" => {},
      } }

      it "is true when the event has the object_kind=pipeline and object_attributes" do
        expect(subject).to be_valid
      end

      it "is false when the event is not for a pipeline event" do
        data["object_kind"] = "commit"
        expect(subject).not_to be_valid
      end

      it "is false when the event is missing the object_kind" do
        data.delete("object_kind")
        expect(subject).not_to be_valid
      end

      it "is false when the event is missing the object_attributes" do
        data.delete("object_attributes")
        expect(subject).not_to be_valid
      end
    end

    describe "#ignore?" do
      it "is false when the event is a build success event" do
        allow(presenter).to receive(:status).and_return("success")
        expect(subject.ignore?).to be false
      end

      it "is false when the event is a build failed event" do
        allow(presenter).to receive(:status).and_return("failed")
        expect(subject.ignore?).to be false
      end

      it "is true when the event is a build start event" do
        allow(presenter).to receive(:status).and_return("pending")
        expect(subject.ignore?).to be true
      end
    end

    describe "#to_markdown" do
      it "delegates to the presenter" do
        sentinel = double
        expect(presenter).to receive(:to_markdown).with(no_args).and_return sentinel
        expect(subject.to_markdown).to eq sentinel
      end
    end
  end
end
