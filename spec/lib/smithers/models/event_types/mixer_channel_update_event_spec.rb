require "spec_helper"
require_file_under_test!

RSpec.describe Smithers::Models::EventTypes::MixerChannelUpdateEvent do

  describe "#new_webhook_event" do
    it "returns an event initialized with the given parameters and request" do
      event_params = double("Event params")
      request = double("The request")
      event = double("The event")
      expect(described_class::WebhookEvent).to receive(:new)
        .with(event_params: event_params, request: request)
        .and_return(event)
      expect(subject.new_webhook_event(event_params, request)).to eq event
    end
  end

  describe described_class::WebhookEvent do
    let(:channel_id) { 42 }
    let(:data) { double("The request data") }
    let(:request) { double("The request", params: data) }
    let(:event_params) { [
      double(value: "[\"channel:#{channel_id}:update\"]", type: "MixerEventsList"),
    ] }
    let(:presenter) { instance_double(Smithers::Mixer::Presenters::ChannelUpdateEvent, to_markdown: ":)") }

    subject { described_class.new(event_params: event_params, request: request) }

    before do
      allow(Smithers::Mixer::Presenters).to receive(:present_event)
        .with(data)
        .and_return(presenter)
    end

    describe "#authorized?" do
      let(:request_body) { %Q|{"event":"channel:8389395:update","id":"5600313a-622b-4e2d-8775-66b5d40cc614","payload":{"online":true},"sentAt":"2018-03-14T11:27:12.6831014+00:00"}| }
      let(:signature) { "sha384=810FBD8205A222FCF119F9DF3FE3883298E932B021B41D04A808D3CD080D6D232E233C5B6B1DEC269C309B94B0DC259A" }

      before do
        allow(request).to receive(:get_header).with("HTTP_POKER_SIGNATURE")
          .and_return signature
        allow(request).to receive_message_chain(:body, :read).with(no_args)
          .and_return request_body
      end

      it "is true when the request's signature is valid for the event and request" do
        expect(subject).to be_authorized
      end

      it "is false when the request's signature is invalid for the event and request" do
        signature[-1] = "X"
        expect(subject).not_to be_authorized
      end

      it "is false when the request's signature is missing" do
        expect(request).to receive(:get_header).with("HTTP_POKER_SIGNATURE")
          .and_return nil
        expect(subject).not_to be_authorized
      end
    end

    describe "#valid?" do
      let(:data) { {
        "event" => "channel:#{channel_id}:update",
      } }

      it "is true when the event is listening for the identified event" do
        expect(subject).to be_valid
      end

      it "is false when the event is not listening for the identified event" do
        data["event"] = "channel:0:update"
        expect(subject).not_to be_valid
        data["event"] = "channel:#{channel_id}:broadcast"
        expect(subject).not_to be_valid
      end

      it "is false when the request does not identify the event" do
        data.delete("event")
        data["event"] = "channel:0:update"
        expect(subject).not_to be_valid
        data["event"] = "channel:#{channel_id}:broadcast"
        expect(subject).not_to be_valid
      end
    end

    describe "#ignore?" do
      let(:data) { {
        "payload" => { "online" => true }
      } }

      it "is false when the channel just came online" do
        data["payload"] = { "online" => true }
        expect(subject).not_to be_ignore
      end

      it "is true when the channel just went offline" do
        data["payload"] = { "online" => false }
        expect(subject).to be_ignore
      end

      it "is true when the event doesn't pertain to online/offline status" do
        data["payload"] = { "something else" => "Who knows!" }
        expect(subject).to be_ignore
      end

      it "is true when the event has already been received" do
        data["payload"] = { "something else" => "Who knows!" }
        expect(subject).to be_ignore
      end
    end

    describe "#to_markdown" do
      it "delegates to the presenter" do
        sentinel = double
        expect(presenter).to receive(:to_markdown).with(no_args).and_return sentinel
        expect(subject.to_markdown).to eq sentinel
      end
    end
  end
end
