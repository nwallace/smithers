require "spec_helper"
require_file_under_test!

RSpec.describe Smithers::Models::ReactionTypes::PostDiscordMessage do

  let(:reaction_params) {[
    double("Channel ID param", type: "DiscordChannelID", value: "The channel ID"),
  ]}
  let(:event) { double("The event") }

  describe "#new_reaction" do
    it "returns a new reaction with the given params and event" do
      reaction = subject.new_reaction(reaction_params, event)
      expect(reaction.event).to eq event
      expect(reaction.channel_id).to eq "The channel ID"
    end

    it "raises an ArgumentError when a parameter is missing" do
      reaction_params.each do |param|
        expect {
          subject.new_reaction(reaction_params - [param], event)
        }.to raise_error ArgumentError
      end
    end

    it "raises an ArgumentError when an unknown parameter is present" do
      unknown_param = double("Unknown param", type: "DiscordUnknown")
      expect {
        subject.new_reaction(reaction_params << unknown_param, event)
      }.to raise_error ArgumentError
    end
  end

  describe described_class::Reaction do
    describe "#execute!" do
      let(:discord_api) { instance_double(Smithers::Discord::API, post_message: api_response) }
      let(:api_response) { double("The response", success?: true) }

      before { allow(Smithers::Discord::API).to receive(:new).and_return(discord_api) }

      context "with a GitLab pipeline event" do
        let(:event) { double("GitLab pipeline event", ignore?: false, to_markdown: ":)") }

        subject { described_class.new(reaction_params, event) }

        it "posts a message about the event to Discord" do
          expect(Smithers::Discord::API).to receive(:new)
            .with(no_args)
            .and_return(discord_api)
          expect(discord_api).to receive(:post_message)
            .with("The channel ID", ":)")
            .and_return(api_response)
          subject.execute!
        end

        it "does nothing for events to ignore" do
          allow(event).to receive(:ignore?).and_return(true)
          expect(discord_api).not_to receive(:post_message)
          subject.execute!
        end

        it "raises an error when the message post fails" do
          allow(api_response).to receive(:success?).and_return(false)
          allow(api_response).to receive(:code).and_return("400")
          allow(api_response).to receive(:body).and_return(":(")
          expect {
            subject.execute!
          }.to raise_error "Unable to post message to Discord: HTTP 400 - :("
        end
      end
    end
  end
end
