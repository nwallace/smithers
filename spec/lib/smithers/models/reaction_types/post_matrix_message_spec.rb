require "spec_helper"
require_file_under_test!

RSpec.describe Smithers::Models::ReactionTypes::PostMatrixMessage do

  let(:reaction_params) {[
    double("Server param", type: "MatrixServer", value: "https://matrix.example.com"),
    double("Access token param", type: "MatrixAccessToken", value: "The token"),
    double("Room ID param", type: "MatrixRoomID", value: "The room ID"),
  ]}
  let(:event) { double("The event") }

  describe "#new_reaction" do
    it "returns a new reaction with the given params and event" do
      reaction = subject.new_reaction(reaction_params, event)
      expect(reaction.event).to eq event
      expect(reaction.server).to eq "https://matrix.example.com"
      expect(reaction.access_token).to eq "The token"
      expect(reaction.room_id).to eq "The room ID"
    end

    it "raises an ArgumentError when a parameter is missing" do
      reaction_params.each do |param|
        expect {
          subject.new_reaction(reaction_params - [param], event)
        }.to raise_error ArgumentError
      end
    end

    it "raises an ArgumentError when an unknown parameter is present" do
      unknown_param = double("Unknown param", type: "MatrixUnknown")
      expect {
        subject.new_reaction(reaction_params << unknown_param, event)
      }.to raise_error ArgumentError
    end
  end

  describe described_class::Reaction do
    describe "#execute!" do
      let(:matrix_api) { instance_double(Smithers::Matrix::API, post_message: api_response) }
      let(:api_response) { double("The response", success?: true) }

      before { allow(Smithers::Matrix::API).to receive(:new).and_return(matrix_api) }

      context "with a GitLab pipeline event" do
        let(:event) { double("GitLab pipeline event", ignore?: false, to_markdown: ":)") }

        subject { described_class.new(reaction_params, event) }

        it "posts a message about the event to Matrix" do
          expect(Smithers::Matrix::API).to receive(:new)
            .with(server: "https://matrix.example.com", access_token: "The token")
            .and_return(matrix_api)
          expect(matrix_api).to receive(:post_message)
            .with("The room ID", ":)")
            .and_return(api_response)
          subject.execute!
        end

        it "does nothing for events to ignore" do
          allow(event).to receive(:ignore?).and_return(true)
          expect(matrix_api).not_to receive(:post_message)
          subject.execute!
        end

        it "raises an error when the message post fails" do
          allow(api_response).to receive(:success?).and_return(false)
          allow(api_response).to receive(:code).and_return("400")
          allow(api_response).to receive(:body).and_return(":(")
          expect {
            subject.execute!
          }.to raise_error "Unable to post message to Matrix: HTTP 400 - :("
        end
      end
    end
  end
end
