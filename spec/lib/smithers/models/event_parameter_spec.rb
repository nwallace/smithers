require "spec_helper"
require_file_under_test!

RSpec.describe Smithers::Models::EventParameter do

  with_types(described_class, :event_parameter_type) do |described_class|
    describe "#type" do
      it "delegates to the EventParameterType" do
        the_type = double("the type")
        event_parameter_type = double(type: the_type)
        subject = described_class.new(event_parameter_type: event_parameter_type)
        expect(subject.type).to eq the_type
      end
    end
  end
end
