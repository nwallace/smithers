require "spec_helper"
require_file_under_test!

RSpec.describe Smithers::Models::Integration do
  with_types(described_class, :event_type, :event_parameters) do |described_class|
    let(:event_type) { double("The event type") }
    let(:event_parameters) { [double("An event parameter")] }
    let(:request) { double("The request") }

    subject { described_class.new(event_type: event_type, event_parameters: event_parameters) }

    describe "#new_webhook_event" do
      it "delegates to the event_type" do
        event = double("The event")
        expect(event_type).to receive(:new_webhook_event)
          .with(event_parameters, request)
          .and_return(event)
        expect(subject.new_webhook_event(request)).to eq event
      end
    end

    describe "#execute!" do
      # TODO
    end
  end
end
