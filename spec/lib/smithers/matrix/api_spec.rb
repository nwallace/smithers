require "spec_helper"
require_file_under_test!

RSpec.describe Smithers::Matrix::API do

  let(:server)       { env_var(:matrix_server) }
  let(:room_id)      { env_var(:matrix_room_id) }
  let(:access_token) { env_var(:matrix_access_token) }

  subject { described_class.new(server: server, access_token: access_token) }

  describe "#post_message" do
    it "posts a message to the given room" do
      response = subject.post_message(room_id, "Your status report, Mr. Burns: #{Faker::Simpsons.quote}")
      expect(response).to be_success
    end

    it "returns an unsuccessful response if something goes wrong" do
      response = subject.post_message("BOGUS", "If a tree falls in the woods...")
      expect(response).not_to be_success
    end
  end
end
