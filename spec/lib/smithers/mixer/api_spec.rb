require "spec_helper"
require_file_under_test!

RSpec.describe Smithers::Mixer::API do

  subject { described_class.new(client_id: env_var(:mixer_client_id), client_secret: env_var(:mixer_client_secret)) }

  describe "#request_valid?" do
    it "is true when the given signature matches the SHA384 HMAC hexdigest of the given string using the given secret" do
      signature = "5EB3E48ED381446210D527AA1D88D9A5F36C840DD088665F35BEA51D3FA429837430E81973835774CC0AE69EEDE6AAE7"
      string = %Q|{"event":"channel:314:update","id":"96445358-d5b1-417e-a9ac-57f1cb001916","payload":{"broacastId":"9976edaf-c327-4560-a1cb-89425cb1131f"},"sentAt":"2018-02-08T03:28:06.8605874+00:00"}|
      expect(subject.signature_valid?(signature: signature, secret: "verysecret", string: string)).to be true
    end
  end
end
