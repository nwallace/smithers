require "spec_helper"
require_file_under_test!

RSpec.describe Smithers::Mixer::Presenters do

  describe ".present_event" do
    it "returns a Channel Event presenter when given channel update event data" do
      expect(described_class.present_event("event" => "channel:123:update")).to be_a described_class::ChannelUpdateEvent
    end

    it "raises an error when given data for an unsupported event" do
      expect {
        described_class.present_event("event" => "channel:123:bogus")
      }.to raise_error KeyError
    end

    it "raises an error when the given data doesn't include the event type" do
      expect {
        described_class.present_event("bogus" => "channel:123:update")
      }.to raise_error KeyError
    end
  end

  describe described_class::ChannelUpdateEvent do
    let(:channel) { double("Channel response", success?: true, body: {token: "Doof"}.to_json) }
    let(:api) { instance_double(Smithers::Mixer::API) }

    before do
      allow(Smithers::Mixer::API).to receive(:new)
        .with(client_id: env_var(:mixer_client_id), client_secret: env_var(:mixer_client_secret))
        .and_return(api)
      allow(api).to receive(:get_channel).with("12").and_return(channel)
    end

    describe "#to_markdown" do
      it "returns a message linking to the broadcast when the channel has come online" do
        subject = described_class.new("event" => "channel:12:update", "payload" => { "online" => true })
        expect(subject.to_markdown).to eq "**Doof** is streaming! https://mixer.com/Doof"
      end

      it "returns an informative message when the channel has gone offline" do
        subject = described_class.new("event" => "channel:12:update", "payload" => { "online" => false })
        expect(subject.to_markdown).to eq "**Doof** has gone offline. The party is over."
      end
    end
  end
end
