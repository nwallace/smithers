def require_file_under_test!
  require caller.first.gsub(/(?<=\/)spec\/|_spec(?=\.rb)|:.*/, "")
end

def with_types(model_class, *attributes)
  require "dry/types"
  class_with_attributes = Class.new(model_class)
  class_with_attributes.attributes(attributes.zip(Array.new(attributes.size, Dry::Types::Any)).to_h)
  yield(class_with_attributes)
end

module Test
  module GlobalHelpers
    def env_var(name)
      var_name = name.to_s.upcase
      ENV.fetch("TEST_#{var_name}") { ENV.fetch(var_name) }
    end

    def transform_keys(obj, &transformation)
      case obj
      when Hash
        obj.each_with_object({}) do |(key, value), result|
          result[transformation.call(key)] = transform_keys(value, &transformation)
        end
      when Array
        obj.map {|value| transform_keys(value, &transformation)}
      else
        obj
      end
    end
  end
end
