require "table_print"

RSpec::Matchers.define :match_struct do |expected|
  match do |actual|
    values_match?(a_hash_including(expected.to_h), actual.to_h)
  end
  failure_message do |actual|
    differences =
      expected.to_h.each_with_object([]) do |(key, expected_value), ds|
        actual_value = actual.to_h[key]
        unless values_match?(expected_value, actual_value)
          ds << { key: key, expected: expected_value, actual: actual_value }
        end
      end
    "expected structs to match, but found differences:\n#{TablePrint::Printer.new(differences).table_print}"
  end
  failure_message_when_negated do |actual|
    "expected structs not to match, but they did: #{TablePrint::Printer.new([expected.to_h]).table_print}"
  end
end
