module Test
  module DatabaseHelpers
    module_function

    def rom
      Smithers::Container["persistence.rom"]
    end

    def db
      Smithers::Container["persistence.db"]
    end

    {
      services: {
        gitlab: { slug: "gitlab" },
        matrix: { slug: "matrix" },
        discord: { slug: "discord" },
      },
      event_types: {
        gitlab_pipeline: { type: "GitlabPipelineEvent" },
        mixer_channel_update: { type: "MixerChannelUpdateEvent" },
      },
      reaction_types: {
        post_matrix_message: { type: "PostMatrixMessage" },
        post_discord_message: { type: "PostDiscordMessage" },
      },
      [:event_parameter_types, :param_types] => {
        gitlab_secret_token: { type: "GitlabSecretToken" },
        mixer_events_list: { type: "MixerEventsList" },
      },
      [:reaction_parameter_types, :param_types] => {
        matrix_server: { type: "MatrixServer" },
        matrix_room_id: { type: "MatrixRoomID" },
        matrix_access_token: { type: "MatrixAccessToken" },
        discord_channel_id: { type: "DiscordChannelID" },
      },
    }.each do |(table, table_alias), instances|
        inflector = Dry::Inflector.new
        instances.each do |name, query|
          define_method("#{name}_#{inflector.singularize(table_alias || table)}") do
            begin
              db[table].where(query).first!
            rescue Sequel::NoMatchingRow => e
              raise e.class, "Row not found: #{table} #{query.inspect}"
            end
          end
        end
      end
  end
end
