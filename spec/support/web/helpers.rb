module Test
  module WebHelpers
    module_function

    def app
      Smithers::Web.app
    end

    def post_json(route, data)
      post(route, JSON[data], "CONTENT_TYPE" => "application/json")
    end

    def last_response_json
      transform_keys(JSON.parse(last_response.body), &:to_sym)
    end

    def described_route(interpolations={})
      example_description = RSpec.current_example.full_description
      if matches=example_description.match(/(GET|POST|PUT|PATCH|DELETE)\s+(\S+)/)
        matches[2].gsub(
          Regexp.union(interpolations.keys.map(&:inspect)),
          transform_keys(interpolations, &:inspect)
        )
      else
        raise "Unable to parse a route for this example: #{example_description}"
      end
    end
  end
end
