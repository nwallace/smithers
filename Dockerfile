FROM ruby:2.5-alpine

MAINTAINER Nathan Wallace <nathan@wallace.fm>

WORKDIR /smithers

COPY Gemfile Gemfile.lock /smithers/

RUN apk add --update build-base libxml2-dev libxslt-dev postgresql-dev postgresql-client less && \
  gem install bundler && \
  bundle config build.nokogiri --use-system-libraries && \
  bundle install && \
  rm -rf /var/cache/apk/*

VOLUME /smithers

ENTRYPOINT ["shotgun", "-p", "3000", "-o", "0.0.0.0", "config.ru"]

EXPOSE 3000
