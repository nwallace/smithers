module Smithers
  class EventPresenter

    class << self
      def data_paths(paths)
        self._data_paths.merge!(paths)
      end
      def external_data(key, &block)
        self._external_data[key] = block
      end
      attr_accessor :_data_paths
      attr_accessor :_external_data
    end

    def self.inherited(base)
      base._data_paths = {}
      base._external_data = {}
    end

    def initialize(event_data)
      @event_data = event_data
    end

    def data(key)
      if data_paths.has_key?(key)
        data_paths[key].inject(@event_data) do |data, key|
          if key.respond_to?(:call)
            data.instance_exec(&key)
          else
            data.fetch(key)
          end
        end
      else
        instance_exec(&external_data.fetch(key))
      end
    end

    private

    def data_paths
      self.class._data_paths
    end

    def external_data
      self.class._external_data
    end
  end
end
