require "cgi"
require "httparty"
require "json"
require "redcarpet"
require "redcarpet/render_strip"

module Smithers
  module Matrix
    class API

      def initialize(server:, access_token: nil, login: nil)
        @server = server
        if access_token
          @access_token = access_token
        elsif login
          login_response = log_in(login)
          if login_response.success?
            @access_token = JSON.parse(login_response.body).fetch("access_token")
          else
            raise "Login failed: #{login_response.code} #{login_response.body}"
          end
        else
          raise ArgumentError, "Must provide either :access_token string or :login hash"
        end
      end

      def post_message(room_id, message)
        plain_text = Redcarpet::Markdown.new(Redcarpet::Render::StripDown, filter_html: true).render(message).gsub(/<[^>]*>/, "")
        html = Redcarpet::Markdown.new(Redcarpet::Render::HTML).render(message)
        post_with_credentials(
          "#{server}/rooms/#{CGI.escape(room_id)}/send/m.room.message",
          msgtype: "m.text", body: plain_text, format: "org.matrix.custom.html", formatted_body: html,
        )
      end

      private

      attr_reader :server, :access_token

      def post(endpoint, args)
        HTTParty.post(endpoint, args)
      end

      def post_with_credentials(endpoint, body={})
        post(endpoint, query: { access_token: access_token }, body: body.to_json)
      end

      def log_in(user:, password:)
        post("#{server}/login", body: {
          type: "m.login.password", user: user, password: password
        }.to_json)
      end
    end
  end
end
