require "smithers/event_presenter"
require "smithers/mixer/api"

module Smithers
  module Mixer
    module Presenters
      class ChannelUpdateEvent < EventPresenter
        data_paths(
          online?: %w[payload online],
          channel_id: ["event", ->{ split(":")[1] }],
        )
        external_data(:username) do
          resp =
            API.new(client_id: ENV.fetch("MIXER_CLIENT_ID"),
                    client_secret: ENV.fetch("MIXER_CLIENT_SECRET"))
               .get_channel(data(:channel_id))
          raise "Unable to get Mixer channel info for channel #{data(:channel_id)}: #{resp.code} #{resp}" unless resp.success?
          JSON.parse(resp.body).fetch("token")
        end
        def to_markdown
          if data(:online?)
            online_message
          else
            offline_message
          end
        end

        private
        def online_message
          "**#{data(:username)}** is streaming! https://mixer.com/#{data(:username)}"
        end
        def offline_message
          "**#{data(:username)}** has gone offline. The party is over."
        end
      end

      def self.present_event(event_data)
        event_name = event_data.fetch("event")
        key = EVENTS_TO_PRESENTERS.keys.find {|rxp| rxp =~ event_name}
        EVENTS_TO_PRESENTERS.fetch(key).new(event_data)
      end

      EVENTS_TO_PRESENTERS = {
        /channel:\d+:update/ => ChannelUpdateEvent,
      }
    end
  end
end
