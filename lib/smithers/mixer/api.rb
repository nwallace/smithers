require "httparty"
require "openssl"

module Smithers
  module Mixer
    class API

      ENDPOINT = "https://mixer.com/api/v1/"

      def initialize(client_id:, client_secret:)
        @client_id = client_id
        @client_secret = client_secret
      end

      def signature_valid?(signature:, secret:, string:)
        OpenSSL::HMAC.hexdigest("sha384", secret, string).casecmp?(signature)
      end

      def channel_update_event(channel_id)
        "channel:#{channel_id}:update"
      end

      def get_channel(username_or_id)
        # doesn't require authorization
        HTTParty.get(endpoint("channels/#{username_or_id}"))
      end

      def register_hook(events, callback_url)
        send_request(:post, endpoint("hooks"),
                     kind: "web",
                     events: events,
                     url: callback_url,
                     secret: client_secret)
      end

      def renew_hook(hook_id)
        send_request(:post, endpoint("hooks/#{hook_id}/renew"))
      end

      def deregister_hook(hook_id)
        send_request(:post, endpoint("hooks/#{hook_id}/deregister"))
      end

      private

      attr_reader :client_id, :client_secret

      def endpoint(path)
        "#{ENDPOINT}#{path}"
      end

      def send_request(http_method, endpoint, body={})
        HTTParty.public_send(
          http_method, endpoint,
          headers: {
            "Client-ID" => client_id,
            "Authorization" => "Secret #{client_secret}",
          },
          body: body.to_json,
        )
      end
    end
  end
end
