require "httparty"

module Smithers
  module Discord
    class API

      ENDPOINT = "https://discordapp.com/api/v6/"

      def initialize
        @access_token = ENV.fetch("DISCORD_ACCESS_TOKEN")
      end

      def post_message(channel_id, message)
        post_with_credentials("channels/#{channel_id}/messages", content: message)
      end

      private

      attr_reader :access_token

      def post_with_credentials(path, body={})
        HTTParty.post(endpoint(path), headers: {
          "Authorization" => "Bot #{access_token}",
          "User-Agent" => "Smithers (https://smithers.nosuchthingastwo.com, v0.1)",
          "Content-Type" => "application/json",
        }, body: body.to_json)
      end

      def endpoint(path)
        "#{ENDPOINT}#{path}"
      end
    end
  end
end
