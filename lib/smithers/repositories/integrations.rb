require "smithers/repository"
require "smithers/models/integration"

module Smithers
  module Repositories
    class Integrations < Smithers::Repository[:integrations]
      struct_namespace Smithers::Models
    end
  end
end
