require "dry/inflector"
require "smithers/models/reaction_type"
require "smithers/discord/api"

module Smithers
  module Models
    module ReactionTypes
      class PostDiscordMessage < ReactionType

        def new_reaction(reaction_params, event)
          Reaction.new(reaction_params, event)
        end

        class Reaction
          attr_reader :event, :channel_id

          def initialize(reaction_params, event)
            process_reaction_params!(reaction_params)
            @event = event
          end

          def execute!
            return OpenStruct.new(success?: true) if event.ignore?
            discord = Discord::API.new
            response = discord.post_message(channel_id, event.to_markdown)
            raise "Unable to post message to Discord: HTTP #{response.code} - #{response.body}" unless response.success?
          end

          private

          def process_reaction_params!(reaction_params)
            param_types = %w[DiscordChannelID]
            unknown_parameters = reaction_params.reject {|p| param_types.include?(p.type)}
            raise ArgumentError, "Unknown parameter(s): #{unknown_parameters}" if unknown_parameters.any?
            inflector = Dry::Inflector.new
            param_types.each do |param_type|
              param = reaction_params.find {|p| p.type == param_type}
              raise ArgumentError, "Missing required parameter #{param_type}" unless param
              ivar = "@#{inflector.underscore(param_type.sub(/^Discord/, ""))}"
              instance_variable_set(ivar, param.value)
            end
          end
        end
      end
    end
  end
end
