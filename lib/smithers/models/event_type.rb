require "rom/struct"
require "dry/inflector"
require "smithers/models/typed_model"

module Smithers
  module Models
    class EventType < ROM::Struct
      include TypedModel

      def request_params(request)
        raise NotImplementedError
      end

      def authorized?(event_params, request_params)
        raise NotImplementedError
      end

      def request_valid?(event_params, request_params)
        raise NotImplementedError
      end
    end
  end
end
