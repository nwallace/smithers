require "smithers/models/event_type"
require "smithers/gitlab/presenters"

module Smithers
  module Models
    module EventTypes
      class GitlabPipelineEvent < EventType
        def new_webhook_event(event_params, request)
          WebhookEvent.new(event_params: event_params, request: request)
        end

        class WebhookEvent
          attr_reader :event_params, :request_params

          def initialize(event_params:, request:)
            @event_params = event_params
            @request_params = {
              secret_token: request.get_header("HTTP_X_GITLAB_TOKEN"),
              event_data: request.params,
            }
          end

          def authorized?
            event_params.any? do |event_param|
              event_param.type == "GitlabSecretToken" &&
                event_param.value == request_params[:secret_token]
            end
          end

          def valid?
            request_data = request_params[:event_data]
            request_data.has_key?("object_kind") &&
              request_data.has_key?("object_attributes") &&
              request_data["object_kind"] == "pipeline"
          end

          def ignore?
            presenter.status != "success" && presenter.status != "failed"
          end

          def to_markdown
            presenter.to_markdown
          end

          private

          def presenter
            @presenter ||= Gitlab::Presenters.present_event(request_params[:event_data])
          end
        end
      end
    end
  end
end
