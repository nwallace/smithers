require "smithers/models/event_type"
require "smithers/mixer/api"
require "smithers/mixer/presenters"

module Smithers
  module Models
    module EventTypes
      class MixerChannelUpdateEvent < EventType
        def new_webhook_event(event_params, request)
          WebhookEvent.new(event_params: event_params, request: request)
        end

        class WebhookEvent
          attr_reader :event_params, :request

          def initialize(event_params:, request:)
            @event_params = event_params
            @request = request
          end

          def authorized?
            signature_header = request.get_header("HTTP_POKER_SIGNATURE")
            return false unless signature_header
            api.signature_valid?(signature: signature_header.sub(/^sha384=/, ""),
                                 secret: ENV.fetch("MIXER_CLIENT_SECRET"),
                                 string: request.body.read)
          end

          def valid?
            events = event_params.find {|p| p.type == "MixerEventsList"}
            events && JSON.parse(events.value).include?(request.params.fetch("event"))
          end

          def ignore?
            !request.params.fetch("payload").fetch("online", false)
          end

          def to_markdown
            presenter.to_markdown
          end

          private

          def presenter
            @presenter ||= Mixer::Presenters.present_event(request.params)
          end

          def api
            @api ||= Mixer::API.new(client_id: ENV.fetch("MIXER_CLIENT_ID"),
                                    client_secret: ENV.fetch("MIXER_CLIENT_SECRET"))
          end
        end
      end
    end
  end
end
