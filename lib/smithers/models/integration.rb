require "rom/struct"

module Smithers
  module Models
    class Integration < ROM::Struct

      def fetch_event_parameter(parameter_type)
        event_parameters.find do |p|
          case parameter_type
          when String
            p.event_parameter_type.type == parameter_type
          when Numeric
            p.event_parameter_type.id == parameter_type
          when Class
            p.event_parameter_type.is_a?(parameter_type)
          when Hash
            p.event_parameter_type.id == parameter_type.fetch(:id)
          else
            p.event_parameter_type == parameter_type
          end
        end.value
      end

      def fetch_reaction_parameter(parameter_type)
        reaction_parameters.find do |p|
          case parameter_type
          when String
            p.reaction_parameter_type.type == parameter_type
          when Numeric
            p.reaction_parameter_type.id == parameter_type
          when Class
            p.reaction_parameter_type.is_a?(parameter_type)
          when Hash
            p.reaction_parameter_type.id == parameter_type.fetch(:id)
          else
            p.reaction_parameter_type == parameter_type
          end
        end.value
      end

      def event_service
        event_type.service
      end

      def reaction_service
        reaction_type.service
      end

      def new_webhook_event(request_params)
        event_type.new_webhook_event(event_parameters, request_params)
      end

      def execute!(event)
        reaction = reaction_type.new_reaction(reaction_parameters, event)
        reaction.execute!
      end
    end
  end
end
