require "rom/struct"

module Smithers
  module Models
    class EventParameter < ROM::Struct
      def type
        event_parameter_type.type
      end
    end
  end
end
