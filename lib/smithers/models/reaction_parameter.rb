require "rom/struct"

module Smithers
  module Models
    class ReactionParameter < ROM::Struct
      def type
        reaction_parameter_type.type
      end
    end
  end
end
