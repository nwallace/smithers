require "dry/inflector"

module Smithers
  module Models
    module TypedModel
      def self.included(base)
        base.class_eval do
          class << self
            alias :_orig_new :new

            def new(attributes={})
              type = attributes[:type]
              class_name_parts = self.name.split("::")
              unless class_name_parts[0..1] == ["Smithers", "Models"]
                raise "TypedModels must be in the Smithers::Models namespace (#{self.name})"
              end
              inflector = Dry::Inflector.new
              concrete_type_class =
                if class_name_parts.count < 4 && type
                  class_name_parts[2] = inflector.pluralize(class_name_parts[2])
                  class_name_parts << type
                  begin
                    const_get(class_name_parts.join("::"))
                  rescue NameError
                    filepath_parts = class_name_parts.map {|s| inflector.underscore(s)}
                    retry if require filepath_parts.join("/")
                  end
                else
                  self
                end
              concrete_type_class._orig_new(attributes)
            end
          end
        end
      end
    end
  end
end
