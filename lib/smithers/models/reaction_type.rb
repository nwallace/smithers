require "rom/struct"
require "smithers/models/typed_model"

module Smithers
  module Models
    class ReactionType < ROM::Struct
      include TypedModel

      def execute!(reaction_params, event)
        raise NotImplementedError
      end
    end
  end
end
