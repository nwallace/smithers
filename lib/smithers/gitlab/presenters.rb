require "smithers/event_presenter"

module Smithers
  module Gitlab
    module Presenters
      class PipelineEvent < EventPresenter
        data_paths(
          project_name:   %w[project name],
          project_url:    %w[project web_url],
          pipeline_id:    %w[object_attributes id],
          status:         %w[object_attributes status],
          ref:            %w[object_attributes ref],
          sha:            %w[object_attributes sha],
          commit_message: %w[commit message],
          builds:         %w[builds],
        )
        def status
          data(:status)
        end
        def to_markdown
          if status == "success"
            success_message
          else
            failure_message
          end
        end

        private
        def success_message
          "*<font color='seagreen'>Build succeeded:</font>* Pipeline for #{data(:project_name)} on #{data(:ref)} finished. [View pipeline](#{pipeline_url})"
        end

        def failure_message
          "**<font color='red'>Build failed:</font>** Pipeline for **#{data(:project_name)}** on *#{data(:ref)}* <font data-mx-bg-color='red'>failed</font>#{" in the *#{stage}* stage" if stage}.\n\nCommit: #{data(:sha)[0..7]} - #{data(:commit_message)}\n\n[View job](#{job_url})"
        end

        def failed_build
          @failed_build ||=
            data(:builds)
              .select {|build| build["status"] == "failed" }
              .max_by {|build| build["id"] }
        end

        def stage
          failed_build && failed_build["stage"]
        end

        def job_id
          failed_build && failed_build["id"]
        end

        def job_url
          project_url = data(:project_url)
          "#{project_url}#{"/" unless project_url.end_with?("/")}-/jobs/#{job_id}"
        end

        def pipeline_url
          project_url = data(:project_url)
          "#{project_url}#{"/" unless project_url.end_with?("/")}pipelines/#{data(:pipeline_id)}"
        end
      end

      def self.present_event(event_data)
        object_kind = event_data.fetch("object_kind")
        OBJECT_KINDS_TO_PRESENTERS.fetch(object_kind).new(event_data)
      end

      OBJECT_KINDS_TO_PRESENTERS = {
        "pipeline" => PipelineEvent,
      }
    end
  end
end
