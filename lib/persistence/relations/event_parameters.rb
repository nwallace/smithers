module Persistence
  module Relations
    class EventParameters < ROM::Relation[:sql]
      schema(:event_parameters, infer: true) do
        associations do
          belongs_to :integration
          belongs_to :event_parameter_type
        end
      end
      auto_struct true
    end
  end
end
