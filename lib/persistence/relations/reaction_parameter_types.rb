module Persistence
  module Relations
    class ReactionParameterTypes < ROM::Relation[:sql]
      schema(:reaction_parameter_types, infer: true) do
        associations do
          belongs_to :reaction_type
        end
      end
      auto_struct true
    end
  end
end
