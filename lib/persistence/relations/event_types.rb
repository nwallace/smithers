module Persistence
  module Relations
    class EventTypes < ROM::Relation[:sql]
      schema(:event_types, infer: true) do
        associations do
          belongs_to :service
          has_many :event_parameter_types
          has_many :integrations
        end
      end
      auto_struct true
    end
  end
end
