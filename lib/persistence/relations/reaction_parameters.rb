module Persistence
  module Relations
    class ReactionParameters < ROM::Relation[:sql]
      schema(:reaction_parameters, infer: true) do
        associations do
          belongs_to :integration
          belongs_to :reaction_parameter_type
        end
      end
      auto_struct true
    end
  end
end
