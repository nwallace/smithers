module Persistence
  module Relations
    class EventParameterTypes < ROM::Relation[:sql]
      schema(:event_parameter_types, infer: true) do
        associations do
          belongs_to :event_type
        end
      end
      auto_struct true
    end
  end
end
