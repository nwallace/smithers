module Persistence
  module Relations
    class ReactionTypes < ROM::Relation[:sql]
      schema(:reaction_types, infer: true) do
        associations do
          belongs_to :service
        end
      end
      auto_struct true
    end
  end
end
