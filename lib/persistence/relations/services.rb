module Persistence
  module Relations
    class Services < ROM::Relation[:sql]
      schema(:services, infer: true)
      auto_struct true
    end
  end
end
