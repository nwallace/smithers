module Persistence
  module Relations
    class Integrations < ROM::Relation[:sql]
      schema(:integrations, infer: true) do
        associations do
          belongs_to :event_type
          belongs_to :reaction_type
          has_many :event_parameters
          has_many :reaction_parameters
        end
      end
      auto_struct true

      def by_id(integration_id)
        by_pk(integration_id)
          .combine(
            event_type: :service,
            reaction_type: :service,
            event_parameters: { event_parameter_type: { event_type: :service } },
            reaction_parameters: { reaction_parameter_type: { reaction_type: :service } },
          ).one
      end
    end
  end
end
