require "dry/inflector"


def insert_records(records_hash, additional_attrs={})
  records_hash.each do |relation, records|
    records.each do |attributes|
      associated_records = attributes.delete(:associated_records)
      attributes.merge!(additional_attrs)
      id = Smithers::Container["persistence.db"][relation].insert(attributes)
      if associated_records && associated_records.any?
        foreign_key_name = "#{Dry::Inflector.new.singularize(relation.to_s)}_id"
        insert_records(associated_records, foreign_key_name.to_sym => id)
      end
    end
  end
end

Smithers::Container["persistence.db"].transaction do
  insert_records({
    services: [
      {
        name: "GitLab",
        slug: "gitlab",
        associated_records: {
          event_types: [{
            type: "GitlabPipelineEvent",
            inlet: "webhook",
            associated_records: {
              event_parameter_types: [{
                type: "GitlabSecretToken",
              }],
            },
          }],
        },
      },
      {
        name: "Matrix",
        slug: "matrix",
        associated_records: {
          reaction_types: [{
            type: "PostMatrixMessage",
            associated_records: {
              reaction_parameter_types: [{
                type: "MatrixServer",
              }, {
                type: "MatrixRoomID",
              }, {
                type: "MatrixAccessToken",
              }],
            },
          }],
        },
      },
      {
        name: "Mixer",
        slug: "mixer",
        associated_records: {
          event_types: [{
            type: "MixerChannelUpdateEvent",
            inlet: "webhook",
            associated_records: {
              event_parameter_types: [{
                type: "MixerEventsList",
              }, {
                type: "MixerHookID",
              }, {
                type: "MixerHookExpiresAt",
              }],
            },
          }],
        },
      },
      {
        name: "Discord",
        slug: "discord",
        associated_records: {
          reaction_types: [{
            type: "PostDiscordMessage",
            associated_records: {
              reaction_parameter_types: [{
                type: "DiscordChannelID",
              }],
            },
          }],
        },
      },
    ],
  })
end
