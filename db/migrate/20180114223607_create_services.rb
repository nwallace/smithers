ROM::SQL.migration do
  change do
    create_table(:services) do
      primary_key :id
      column :name, String, null: false
      column :slug, String, null: false, unique: true
      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table(:connections) do
      primary_key :id
      foreign_key :service_id, :services, null: false
      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table(:connection_parameters) do
      primary_key :id
      foreign_key :connection_id, :connections, null: false
      column :key, String, null: false
      column :value, String, null: false
      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end
  end
end
