ROM::SQL.migration do
  up do
    drop_table(:connection_parameters, :connections)

    create_table(:event_types) do
      primary_key :id
      foreign_key :service_id, :services, null: false
      column :type, String, null: false
      column :inlet, String, null: false
      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table(:reaction_types) do
      primary_key :id
      foreign_key :service_id, :services, null: false
      column :type, String, null: false
      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table(:event_parameter_types) do
      primary_key :id
      foreign_key :event_type_id, :event_types, null: false
      column :type, String, null: false, index: true
      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table(:reaction_parameter_types) do
      primary_key :id
      foreign_key :reaction_type_id, :reaction_types, null: false
      column :type, String, null: false, index: true
      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table(:integrations) do
      primary_key :id
      foreign_key :event_type_id, :event_types, null: false
      foreign_key :reaction_type_id, :reaction_types, null: false
      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table(:event_parameters) do
      primary_key :id
      foreign_key :integration_id, :integrations, null: false, index: true
      foreign_key :event_parameter_type_id, :event_parameter_types, null: false
      column :value, String
      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table(:reaction_parameters) do
      primary_key :id
      foreign_key :integration_id, :integrations, null: false, index: true
      foreign_key :reaction_parameter_type_id, :reaction_parameter_types, null: false
      column :value, String
      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end
  end

  down do
    create_table(:connections) do
      primary_key :id
      foreign_key :service_id, :services, null: false
      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table(:connection_parameters) do
      primary_key :id
      foreign_key :connection_id, :connections, null: false
      column :key, String, null: false
      column :value, String, null: false
      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    drop_table(:event_parameters, :reaction_parameters, :event_parameter_types, :reaction_parameter_types, :integrations, :event_types, :reaction_types)
  end
end
