--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.2
-- Dumped by pg_dump version 10.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: event_parameter_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.event_parameter_types (
    id integer NOT NULL,
    event_type_id integer NOT NULL,
    type text NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: event_parameter_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.event_parameter_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: event_parameter_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.event_parameter_types_id_seq OWNED BY public.event_parameter_types.id;


--
-- Name: event_parameters; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.event_parameters (
    id integer NOT NULL,
    integration_id integer NOT NULL,
    event_parameter_type_id integer NOT NULL,
    value text,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: event_parameters_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.event_parameters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: event_parameters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.event_parameters_id_seq OWNED BY public.event_parameters.id;


--
-- Name: event_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.event_types (
    id integer NOT NULL,
    service_id integer NOT NULL,
    type text NOT NULL,
    inlet text NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: event_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.event_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: event_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.event_types_id_seq OWNED BY public.event_types.id;


--
-- Name: integrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.integrations (
    id integer NOT NULL,
    event_type_id integer NOT NULL,
    reaction_type_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: integrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.integrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: integrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.integrations_id_seq OWNED BY public.integrations.id;


--
-- Name: reaction_parameter_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.reaction_parameter_types (
    id integer NOT NULL,
    reaction_type_id integer NOT NULL,
    type text NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: reaction_parameter_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.reaction_parameter_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reaction_parameter_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.reaction_parameter_types_id_seq OWNED BY public.reaction_parameter_types.id;


--
-- Name: reaction_parameters; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.reaction_parameters (
    id integer NOT NULL,
    integration_id integer NOT NULL,
    reaction_parameter_type_id integer NOT NULL,
    value text,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: reaction_parameters_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.reaction_parameters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reaction_parameters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.reaction_parameters_id_seq OWNED BY public.reaction_parameters.id;


--
-- Name: reaction_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.reaction_types (
    id integer NOT NULL,
    service_id integer NOT NULL,
    type text NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: reaction_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.reaction_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reaction_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.reaction_types_id_seq OWNED BY public.reaction_types.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    filename text NOT NULL
);


--
-- Name: services; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.services (
    id integer NOT NULL,
    name text NOT NULL,
    slug text NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: services_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: services_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.services_id_seq OWNED BY public.services.id;


--
-- Name: event_parameter_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.event_parameter_types ALTER COLUMN id SET DEFAULT nextval('public.event_parameter_types_id_seq'::regclass);


--
-- Name: event_parameters id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.event_parameters ALTER COLUMN id SET DEFAULT nextval('public.event_parameters_id_seq'::regclass);


--
-- Name: event_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.event_types ALTER COLUMN id SET DEFAULT nextval('public.event_types_id_seq'::regclass);


--
-- Name: integrations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.integrations ALTER COLUMN id SET DEFAULT nextval('public.integrations_id_seq'::regclass);


--
-- Name: reaction_parameter_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reaction_parameter_types ALTER COLUMN id SET DEFAULT nextval('public.reaction_parameter_types_id_seq'::regclass);


--
-- Name: reaction_parameters id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reaction_parameters ALTER COLUMN id SET DEFAULT nextval('public.reaction_parameters_id_seq'::regclass);


--
-- Name: reaction_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reaction_types ALTER COLUMN id SET DEFAULT nextval('public.reaction_types_id_seq'::regclass);


--
-- Name: services id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.services ALTER COLUMN id SET DEFAULT nextval('public.services_id_seq'::regclass);


--
-- Name: event_parameter_types event_parameter_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.event_parameter_types
    ADD CONSTRAINT event_parameter_types_pkey PRIMARY KEY (id);


--
-- Name: event_parameters event_parameters_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.event_parameters
    ADD CONSTRAINT event_parameters_pkey PRIMARY KEY (id);


--
-- Name: event_types event_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.event_types
    ADD CONSTRAINT event_types_pkey PRIMARY KEY (id);


--
-- Name: integrations integrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.integrations
    ADD CONSTRAINT integrations_pkey PRIMARY KEY (id);


--
-- Name: reaction_parameter_types reaction_parameter_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reaction_parameter_types
    ADD CONSTRAINT reaction_parameter_types_pkey PRIMARY KEY (id);


--
-- Name: reaction_parameters reaction_parameters_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reaction_parameters
    ADD CONSTRAINT reaction_parameters_pkey PRIMARY KEY (id);


--
-- Name: reaction_types reaction_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reaction_types
    ADD CONSTRAINT reaction_types_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (filename);


--
-- Name: services services_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.services
    ADD CONSTRAINT services_pkey PRIMARY KEY (id);


--
-- Name: services services_slug_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.services
    ADD CONSTRAINT services_slug_key UNIQUE (slug);


--
-- Name: event_parameter_types_type_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX event_parameter_types_type_index ON public.event_parameter_types USING btree (type);


--
-- Name: event_parameters_integration_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX event_parameters_integration_id_index ON public.event_parameters USING btree (integration_id);


--
-- Name: reaction_parameter_types_type_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX reaction_parameter_types_type_index ON public.reaction_parameter_types USING btree (type);


--
-- Name: reaction_parameters_integration_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX reaction_parameters_integration_id_index ON public.reaction_parameters USING btree (integration_id);


--
-- Name: event_parameter_types event_parameter_types_event_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.event_parameter_types
    ADD CONSTRAINT event_parameter_types_event_type_id_fkey FOREIGN KEY (event_type_id) REFERENCES public.event_types(id);


--
-- Name: event_parameters event_parameters_event_parameter_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.event_parameters
    ADD CONSTRAINT event_parameters_event_parameter_type_id_fkey FOREIGN KEY (event_parameter_type_id) REFERENCES public.event_parameter_types(id);


--
-- Name: event_parameters event_parameters_integration_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.event_parameters
    ADD CONSTRAINT event_parameters_integration_id_fkey FOREIGN KEY (integration_id) REFERENCES public.integrations(id);


--
-- Name: event_types event_types_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.event_types
    ADD CONSTRAINT event_types_service_id_fkey FOREIGN KEY (service_id) REFERENCES public.services(id);


--
-- Name: integrations integrations_event_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.integrations
    ADD CONSTRAINT integrations_event_type_id_fkey FOREIGN KEY (event_type_id) REFERENCES public.event_types(id);


--
-- Name: integrations integrations_reaction_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.integrations
    ADD CONSTRAINT integrations_reaction_type_id_fkey FOREIGN KEY (reaction_type_id) REFERENCES public.reaction_types(id);


--
-- Name: reaction_parameter_types reaction_parameter_types_reaction_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reaction_parameter_types
    ADD CONSTRAINT reaction_parameter_types_reaction_type_id_fkey FOREIGN KEY (reaction_type_id) REFERENCES public.reaction_types(id);


--
-- Name: reaction_parameters reaction_parameters_integration_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reaction_parameters
    ADD CONSTRAINT reaction_parameters_integration_id_fkey FOREIGN KEY (integration_id) REFERENCES public.integrations(id);


--
-- Name: reaction_parameters reaction_parameters_reaction_parameter_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reaction_parameters
    ADD CONSTRAINT reaction_parameters_reaction_parameter_type_id_fkey FOREIGN KEY (reaction_parameter_type_id) REFERENCES public.reaction_parameter_types(id);


--
-- Name: reaction_types reaction_types_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reaction_types
    ADD CONSTRAINT reaction_types_service_id_fkey FOREIGN KEY (service_id) REFERENCES public.services(id);


--
-- PostgreSQL database dump complete
--

