# Smithers

Welcome! You’ve generated an app using dry-web-roda.

## First steps

1. Run `bundle`
1. Review `.env` & `.env.test` (and make a copy to e.g. `.example.env` if you want example settings checked in)
1. Run `bundle exec rake db:create`
1. Add your own steps to `bin/setup`
1. Run the app with `bundle exec shotgun -p 3000 -o 0.0.0.0 config.ru`
1. Initialize git with `git init` and make your initial commit

## Notes


### Discord Setup

* Invite Smithers to a Discord App with this link: https://discordapp.com/oauth2/authorize?&client_id=423982494841438218&scope=bot&permissions=0
* To get your chat channel ID, enable developer mode (User Settings > Appearance > Developer Mode), then right-click the channel and copy ID
* To be able to post messages to the chat channel, your bot must have connected to the Discord Gateway at least once. You can do that by running this code in the Chrome console: ([credit](http://www.scooterx3.net/2016-05-25/discord-bot.html))

```
var ws = new WebSocket('wss://gateway.discord.gg');

mystring = JSON.stringify({
  "op": 2,
  "d": {
    "token": "<bot token>",
    "properties": {
      "$os": "linux",
      "$browser": "sometestingbrowser",
      "$device": "sometestingdevice",
      "$referrer": "",
      "$referring_domain": "",
    },
    "compress": true,
    "large_threshold": 250,
  }
})

ws.send(mystring)
```
