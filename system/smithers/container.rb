require "dry/web/container"
require "dry/system/components"
require "dotenv"

Dotenv.overload(".env", ".env.#{ENV["RACK_ENV"]}")

module Smithers
  class Container < Dry::Web::Container
    configure do
      config.name = :smithers
      config.listeners = true
      config.default_namespace = "smithers"
      config.auto_register = %w[lib/smithers]
    end

    load_paths! "lib"
  end
end
