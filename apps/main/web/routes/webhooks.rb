require "json"

class Smithers::Main::Web
  route "api" do |r|
    http_headers = []
    r.each_header {|k,v| http_headers << [k,v] if k.start_with?("HTTP")}
    Smithers::Container[:logger].info("REQUEST HEADERS : #{http_headers.map{|x| x.join(": ")}.join(", ")}\nREQUEST PARAMS : #{r.params}")

    r.is "v1" do
      r.is "webhooks" do
        r.is Integer do |integration_id|
          repo = Smithers::Container["repositories.integrations"].integrations
          integration = repo.by_id(integration_id)
          if integration
            event = integration.new_webhook_event(request)
            r.resolve("transactions.process_webhook_event") do |transaction|
              transaction.call(integration: integration, event: event) do |run|
                run.success do |result|
                  response.status = result[:http_status_code] || 200
                  JSON[{status: "success", message: result[:message]}]
                end
                run.failure do |result|
                  response.status = result[:http_status_code] || 400
                  JSON[{status: "failed", message: result[:message] || "Unknown error"}]
                end
              end
            end
          else
            response.status = 404
            JSON[{status: "failed", message: "Resource does not exist"}]
          end
        end
      end
    end
  end
end
