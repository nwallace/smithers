require "pathname"
require "dry/web/container"
require "dry/system/components"

module Smithers
  module Main
    class Container < Dry::Web::Container
      require root.join("system/smithers/container")
      import core: Smithers::Container

      configure do |config|
        config.root = Pathname(__FILE__).join("../../..").realpath.dirname.freeze
        config.logger = Smithers::Container[:logger]
        config.default_namespace = "smithers.main"
        config.auto_register = %w[lib/smithers/main]
      end

      load_paths! "lib"
    end
  end
end
