require "smithers/main/transaction"

module Smithers
  module Main
    module Transactions
      class ProcessWebhookEvent < Transaction
        step :authorize, with: "operations.webhooks.authorize"
        step :validate,  with: "operations.webhooks.validate"
        step :execute,   with: "operations.webhooks.execute"
      end
    end
  end
end
