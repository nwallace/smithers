require "smithers/operation"

module Smithers
  module Main
    module Operations
      module Webhooks
        class Authorize < Operation
          def call(integration:, event:)
            if event.authorized?
              Right(integration: integration, event: event)
            else
              Left(http_status_code: 401, message: "Unauthorized")
            end
          end
        end
      end
    end
  end
end
