require "smithers/operation"

module Smithers
  module Main
    module Operations
      module Webhooks
        class Execute < Operation
          def call(integration:, event:)
            integration.execute!(event)
            Right(http_status_code: 200, message: "Complete")
          rescue => e
            Left(http_status_code: 500, message: "Unexpected error: #{e.class} - #{e.message}")
          end
        end
      end
    end
  end
end
