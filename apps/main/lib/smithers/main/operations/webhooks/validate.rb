require "smithers/operation"

module Smithers
  module Main
    module Operations
      module Webhooks
        class Validate < Operation
          def call(integration:, event:)
            if event.valid?
              Right(integration: integration, event: event)
            else
              Left(http_status_code: 400, message: "Bad request")
            end
          end
        end
      end
    end
  end
end
