require "dry/transaction"
require "smithers/main/container"

module Smithers
  module Main
    class Transaction
      include Dry::Transaction(container: Container)
    end
  end
end
